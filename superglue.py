import pandas as pd
import numpy as np
import csv as cv
import logging

CSVS = ['session_1.csv', 'session_2.csv']

file_num = len(CSVS)

# Read the CSVS as a pandas dataframe and put them into a list:
composite_reward_side = []  # Holds the "total" reward_side
for index, csv in enumerate(CSVS):
    try:
        dt = pd.read_csv(csv, skiprows=6, sep=';')
    except FileNotFoundError:
        logging.critical('Error: CSV file/s not found. Exiting...')
        raise
    trial_num = len(dt.query("TYPE=='TRIAL' and MSG=='New trial'"))
    if index != file_num - 1: # We always lose the last trial except in the last CSV file (assuming the last session
    # ended correctly, which is a pretty safe assumption ... )
        trial_num -= 1 
    # Recover the whole reward_side as a list of integers:
    reward_side = list(map(int, dt[dt.MSG == "REWARD_SIDE"]['+INFO'].iloc[-1][1:-1].split(',')))
    # Split it properly and add it to the rest:
    composite_reward_side = np.concatenate((composite_reward_side, reward_side[:trial_num]), axis=None)
    if index == 0:
        # Get the absolute_time, the time that comes from the last valid trial of the first session
        # that has to be added to the rest of the sessions.
        # The INFO lines contain the absolute time for a trial in the BPOD session,
        # but they are at the middle of the trial information. We need to check first if the cut 
        # was produced after last_info but before the start of the next trial, because in that case
        # the last trial information is not complete and we need to discard it.
        last_info = dt.query("TYPE=='INFO' and MSG=='TRIAL-BPOD-TIME'")
        last_trial = dt.query("TYPE=='TRIAL' and MSG=='New trial'").index[-1]
        dt = dt[:last_trial]
        if last_info.index[-1] > last_trial:
            last_absolute_time = float(last_info['BPOD-FINAL-TIME'].values[-2])
        else:
            last_absolute_time = float(last_info['BPOD-FINAL-TIME'].values[-1])
        glued_dataframe = dt
        try:
            session_name = dt[dt.MSG == 'SESSION-NAME']['+INFO'].iloc[0]
        except IndexError:
            session_name = "Unknown session"
            logging.warning("Session name not found. Saving as 'Unknown session'.")
    else:
        # Now dataframe_1 contains the first dataframe cut at an adequate position to append
        # the next session (but it doesn't contain the header, the first six lines that we cut before).
        # Now we cut the second one, starting with the first new trial:
        first_trial = dt.query("TYPE=='TRIAL' and MSG=='New trial'").index[0]
        dt = dt[first_trial:]
        # We add the absolute times to the relevant places:
        dt.loc[dt['TYPE'] == 'INFO', 'BPOD-INITIAL-TIME'] += last_absolute_time 
        dt.loc[dt['TYPE'] == 'INFO', 'BPOD-FINAL-TIME'] += last_absolute_time
        # Append and save:
        glued_dataframe = pd.concat((glued_dataframe, dt), ignore_index=True)
# Replace the old reward_side:
idx = glued_dataframe.query("TYPE=='VAL' and MSG=='REWARD_SIDE'")['+INFO'].index
glued_dataframe.loc[idx, '+INFO'] = str(list(map(int, list(composite_reward_side))))  # Don't ask please
# Recover the first six lines:
finished_filename = str(session_name) + "_GLUED.csv"
with open(CSVS[0], 'r') as first_doc:
    lines = first_doc.readlines()[:6]
    with open(finished_filename, 'w') as finished_doc:
        finished_doc.writelines(lines)
glued_dataframe.to_csv(finished_filename, mode='a', sep=';', quoting=cv.QUOTE_NONE, index=False, quotechar='', escapechar='')
